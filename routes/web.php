<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/home', function () {
    return view('home');
});

Auth::routes();

Route::get('/', 'HomeController@index');
Route::post('/todo', 'TodoController@add_new_todo')->middleware('auth');
Route::put('/todo', 'TodoController@update_todo')->middleware('auth');
Route::delete('/todo', 'TodoController@delete_todo')->middleware('auth');
Route::get('/todos', 'TodoController@get_all_todos')->middleware('auth');
