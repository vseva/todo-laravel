var React = require('react');
var ReactDOM = require('react-dom');
var $ = require('jquery');

var TodoList = require('./components/TodoList');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});

ReactDOM.render(<TodoList />, document.getElementById('app'));


