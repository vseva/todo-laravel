/**
 * Created by sevadenisov on 25/09/16.
 */

var React = require('react');
var $ = require('jquery');

module.exports = React.createClass({
    getInitialState: function() {
        return {
            completed: this.props.data.completed || false,
            content: this.props.data.content || '',
            id: this.props.data.id,
            token: this.props.token,
            disabled: false,
            notDeleted: true
        }
    },

    render: function () {
        return (
            <form action="/todo" className={this.state.notDeleted}>
                <input type="hidden" name="id" value={this.state.id}/>
                <input type="hidden" name="_token" value={this.state.token}/>
                <input type="checkbox"
                       checked={ this.state.completed }
                       name="completed"
                       value="completed"
                       onChange={this.onCompletedChangeHandler} />
                <input type="text"
                       name="content"
                       value={this.state.content}
                       onChange={this.onContentChangeHandler}/>
                <button onClick={this.onUpdate}
                        disabled={this.state.updateDisabled}>Update</button>
                <button onClick={this.onDelete}
                        disabled={this.state.deleteDisabled}>Delete</button>
            </form>
        );
    },

    onCompletedChangeHandler: function(e) {
        var isChecked = $(e.target).is(':checked');

        this.setState({
            completed: isChecked
        });
    },

    onContentChangeHandler: function(e) {
        this.setState({
            content: e.target.value
        });
    },

    onUpdate: function(e) {
        e.preventDefault();

        this.setState({
            updateDisabled: true
        });

        $.ajax({
            url: '/todo',
            type: 'put',
            data: $(e.target).closest('form').serialize(),
            success: function(data) {
                this.setState({
                    updateDisabled: false
                });
            }.bind(this)
        });
    },

    onDelete: function(e) {
        e.preventDefault();

        this.setState({
            deleteDisabled: true
        });

        $.ajax({
            url: '/todo',
            type: 'delete',
            data: $(e.target).closest('form').serialize(),
            success: function(data) {
                this.setState({
                    notDeleted: 'hidden'
                });
            }.bind(this)
        });
    }
});
