/**
 * Created by sevadenisov on 25/09/16.
 */

var React = require('react');
var $ = require('jquery');

module.exports = React.createClass({
    getInitialState: function() {
        return {
            content: '',
            token: window.Laravel.csrfToken,
            todos: this.props.todos
        }
    },

    render: function () {
        return (
            <div className="add-todo">
                <hr/>
                <h3>Add New Todo</h3>
                <form method="post" action="/todo">
                    <input type="hidden"
                           name="_token"
                           value={this.state.token}/>
                    <input type="hidden"
                           name="completed"
                           value=""/>
                    <input type="text"
                           name="content"
                           value={this.state.content}
                           onChange={this.onChangeHandler} />
                    <button onClick={this.onSubmitHandler}>Add Todo</button>
                </form>
            </div>
        );
    },

    onChangeHandler: function(e) {
        this.setState({
            content: e.target.value
        });
    },

    onSubmitHandler: function(e) {
        e.preventDefault();

        var $form = $(e.target).closest('form');

        $.ajax({
            type: 'post',
            url: '/todo',
            data: $form.serialize(),
            success: function(data) {
                var id = data.id;

                var newTodos = this.state.todos;

                newTodos.push({
                    id: id,
                    completed: false,
                    content: this.state.content
                });

                this.setState({
                    todos: newTodos,
                    content: ''
                });
            }.bind(this)
        });
    }
});
