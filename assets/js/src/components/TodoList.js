/**
 * Created by sevadenisov on 25/09/16.
 */

var React = require('react');
var $ = require('jquery');

var TodoItem = require('./TodoItem');
var AddTodo = require('./AddTodo');

module.exports = React.createClass({
    getInitialState: function() {
        return {
            todos: [],
            token: window.Laravel.csrfToken
        }
    },

    componentDidMount: function() {
        this.serverRequest = $.ajax({
            url: '/todos',
            success: function(todos) {
                this.setState({
                    todos: todos
                })
            }.bind(this)
        });
    },

    render: function () {
        return (
            <div className="todo-list">
                <h2>My Todos</h2>
                <div className="js-todo-list">
                    {this.state.todos.map(function(todo) {
                        return <TodoItem key={todo.id}
                                         token={this.state.token}
                                         data={todo} />;
                    }.bind(this))}
                </div>

                <AddTodo todos={this.state.todos} />
            </div>

        );
    },

    onChangeHandler: function(e) {
        this.setState({
            content: e.target.value
        });
    },

    onSubmitHandler: function(e) {
        e.preventDefault();

        var $form = $(e.target).closest('form');

        $.ajax({
            type: 'post',
            url: '/todo',
            data: $form.serialize(),
            success: function(data) {
                var id = data.id;

                var newTodos = this.state.todos;

                newTodos.push({
                    id: id,
                    completed: false,
                    content: this.state.content
                });

                this.setState({
                    todos: newTodos,
                    content: ''
                });
            }.bind(this)
        });
    }
});
