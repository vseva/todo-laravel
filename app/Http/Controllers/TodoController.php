<?php
/**
 * Created by PhpStorm.
 * User: sevadenisov
 * Date: 25/09/16
 * Time: 13:23
 */

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use App\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function add_new_todo(
        Request $request
    )
    {
        $todo = new Todo;
        $user = $request->user();

        $todo->user_id = $user->id;
        $todo->completed = $request->input('completed');
        $todo->content = $request->input('content');
        $todo->save();

        return array(
            'status' => 'success',
            'id' => $todo->id
        );
    }

    public function get_all_todos(
        Request $request
    ) {
        $user = $request->user();
        $todos = Todo::where('user_id', $user->id)->get();

        return $todos;
    }

    public function update_todo(
        Request $request
    ) {
        $todo = Todo::find($request->input('id'));

        $todo->completed = $request->input('completed');
        $todo->content= $request->input('content');

        $todo->save();

        return $request->input('id');
    }

    public function delete_todo(
        Request $request
    ) {
        $todo = Todo::find($request->input('id'));

        $todo->delete();

        return array(
            'status' => 'success'
        );
    }
}
